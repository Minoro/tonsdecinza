import java.rmi.Naming;
import java.rmi.RemoteException;


public class ServerCaller implements Runnable{

	private int []imagem;
	private int height, width;
	private int index;
	private Conversor server = null;
	
	public ServerCaller(int[] imagem, int height, int width, String server_name, int index) {
		this.imagem = imagem; 
		this.height = height;
		this.width = width;
		this.index = index;
		
		String fullname = server_name;
		
		try {
			System.out.println("Nome = " + fullname);
			server = (Conversor) Naming.lookup(fullname);
			
		} catch (Exception e) {
			System.out.println("Erro ao carregar o server "
					+ fullname + ": " + e);
			System.exit(-1);
		}
	}
	
	@Override
	public void run() {	
		int[] imagem_cinza;
		Container imagem_tons_cinza = new Container();
		try {
//			imagem_cinza = server.transformarEmCinza(this.imagem, this.height, this.width);
//			Utils.parte_imagens_cinza.add(imagem_cinza);
			imagem_tons_cinza.index = this.index;
			imagem_tons_cinza.parte_imagem = server.transformarEmCinza(this.imagem, this.height, this.width);
			
			Utils.parte_imagens_cinza.add(imagem_tons_cinza);
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}	
	
}
