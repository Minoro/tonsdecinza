

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Conversor extends Remote{
	
		int[] transformarEmCinza(int[] image, int height, int width) throws RemoteException;
}
