

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class TonsDeCinza extends UnicastRemoteObject implements Conversor{
	
	private static final long serialVersionUID = 1L;

	protected TonsDeCinza() throws RemoteException {
	}
	
	@Override
	public int[] transformarEmCinza(int[] image, int height, int width){
		System.out.println("Transformando em cinza...");
		BufferedImage imagem = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		imagem.setRGB(0, 0, width, height, image, 0, width);
		for (int i = 0; i < imagem.getWidth(); i++) {
			for (int j = 0; j < imagem.getHeight(); j++) {
				Color rgb = new Color(imagem.getRGB(i, j));
				
				int media = (rgb.getRed()+rgb.getGreen()+rgb.getBlue())/3;
				
				Color mediaRGB = new Color(media,media,media );
				imagem.setRGB(i, j, mediaRGB.getRGB());
			}
		}
		imagem.getRGB(0, 0, imagem.getWidth(), imagem.getHeight(), image, 0, imagem.getWidth());
		
		return image;
	}
		
	public static void main(String[] args){
		
		try {
			String name = args[0];
			TonsDeCinza cinza = new TonsDeCinza();
			
			Naming.rebind(name, cinza);
		} catch (Exception e) {
			System.out.println("Erro!");
			e.printStackTrace();
		}
		
	}

	
	
}
