import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.ObjectInputStream.GetField;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.imageio.ImageIO;

public class CinzaCliente {
	public static void main(String[] args) {
		// install RMI security manager
		System.setSecurityManager(new RMISecurityManager());

		final List<Thread> tarefas = new ArrayList<>(args.length);

		try {

			File arquivoImagem = new File("/home/minoro/Documentos/Java/Imagens/entrada.jpg");
			BufferedImage imagem = ImageIO.read(arquivoImagem);
	
			BufferedImage imagens[] = cortarImagem(imagem, 1, args.length);
			
			int i = 0;
			for (BufferedImage bufferedImage : imagens) {
				int imagemArray[] = new int[bufferedImage.getWidth()* bufferedImage.getHeight()];
				bufferedImage.getRGB(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), imagemArray, 0, bufferedImage.getWidth());

				Thread t = new Thread(new ServerCaller(imagemArray, bufferedImage.getHeight(),bufferedImage.getWidth(), args[i], i));
				t.start();
				
				tarefas.add(t);
				
				i++;
			}
			for (Thread thread : tarefas) {
				thread.join();
			}

//			i = 0;
//			for(int[] val : Utils.parte_imagens_cinza){
//				imagens[i].setRGB(0, 0, imagens[i].getWidth(),imagens[i].getHeight(), val, 0, imagens[0].getWidth());
////				gravarImagem(imagens[i], "/home/minoro/Documentos/Java/Imagens/saida_parte_"+i);
//				
//				i++;
//			}
			
			Collections.sort(Utils.parte_imagens_cinza, new Comparador());
			i = 0;
			for(Container c : Utils.parte_imagens_cinza){
				imagens[i].setRGB(0, 0, imagens[i].getWidth(),imagens[i].getHeight(), Utils.parte_imagens_cinza.get(i).parte_imagem , 0, imagens[0].getWidth());
				i++;
			}
			
			imagem = unirImagem(imagens, 1, args.length);
			
			System.out.println("FIM...");
			gravarImagem(imagem, "/home/minoro/Documentos/Java/Imagens/saida_apos_processo");

		} catch (Exception e) {
			System.out.println("Deu erro com muitas matemáticas: " + e);
			System.exit(-1);
		}
	}
	
	private static BufferedImage[] cortarImagem(BufferedImage image, int rows, int cols){
		int chunks = rows * cols;  
		  
        int chunkWidth = image.getWidth() / cols; // determines the chunk width and height  
        int chunkHeight = image.getHeight() / rows;  
        int count = 0;  
        BufferedImage imgs[] = new BufferedImage[chunks]; //Image array to hold image chunks  
        for (int x = 0; x < rows; x++) {  
            for (int y = 0; y < cols; y++) {  
                //Initialize the image array with image chunks  
                imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());  
                // draws the image chunk  
                Graphics2D gr = imgs[count++].createGraphics();  
                gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x, chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);  
                gr.dispose();  
            }  
        }  
				
		return imgs;
	}
	
	private static BufferedImage unirImagem(BufferedImage imagens[], int rows, int cols){
		 
        int chunks = rows * cols;  
  
        int chunkWidth, chunkHeight;  
        int type;  
       
        type = imagens[0].getType();  
        chunkWidth = imagens[0].getWidth();  
        chunkHeight = imagens[0].getHeight();  
  
        BufferedImage finalImg = new BufferedImage(chunkWidth*cols, chunkHeight*rows, type);  
  
        int num = 0;  
        for (int i = 0; i < rows; i++) {  
            for (int j = 0; j < cols; j++) {  
                finalImg.createGraphics().drawImage(imagens[num], chunkWidth * j, chunkHeight * i, null);  
                num++;  
            }  
        }
		return finalImg;
		
	}
	
	
//	private static BufferedImage[] cortarImagem(BufferedImage image, int linhas, int colunas){
//		BufferedImage imgs[] = new BufferedImage[linhas*colunas];
//		int x = 0, y = 0;
//		for(int count = 0; count < linhas*colunas; count++){
//			imgs[count] = new BufferedImage(linhas, colunas, image.getType());
//			
//			imgs[count] = image.getSubimage(x, y , colunas, linhas);
//			x += colunas;
//			y += linhas;
//			gravarImagem(imgs[count], "/home/minoro/saida_"+count);
//		}
//		
//		return imgs;
//	}

	public static void gravarImagem(BufferedImage img, String nome) {
		// gravando janela
		try {
			new File(nome + ".jpg").mkdirs();
			ImageIO.write(img, "jpg", new File(nome + ".jpg"));
			System.out.print(" = " + nome + ".jpg\n");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

}
